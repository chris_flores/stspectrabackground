#ifndef STAR_StKnockoutProtonMaker_MAKER
#define STAR_StKnockoutProtonMaker_MAKER

#ifndef StMaker_H
#include "StMaker.h"
#endif

//Forward Declarations
class TFile;
class TH1D;
class TH2D;
class TH3D;

class StKnockoutProtonMaker : public StMaker {

 public:
  StKnockoutProtonMaker(char *name);
  ~StKnockoutProtonMaker();

  Int_t Init();
  virtual void Clear(const char *opt="");
  Int_t Make();
  Int_t Finish();
  void SetFileIndex(char *val) {mFileIndex=val;}
  void SetOutDir(char *val) {mOutDir=val;}

 private:
  char *fileNameBase;
  char *mFileIndex;
  char *mOutDir;

  TFile *outFile;

  //We need histograms for each Event Configuration
  TH1D *nEventsHistoColliderCenter;
  TH1D *nEventsHistoColliderPosY;
  TH1D *nEventsHistoColliderNegY;
  
  TH3D *dEdxHistoColliderCenter;
  TH3D *dEdxHistoColliderPosY;
  TH3D *dEdxHistoColliderNegY;
  
  std::vector < std::pair<TH3D *, TH3D *> > protonGlobalDCAHistoColliderCenter;
  std::vector < std::pair<TH3D *, TH3D *> > protonGlobalDCAHistoColliderPosY;
  std::vector < std::pair<TH3D *, TH3D *> > protonGlobalDCAHistoColliderNegY;

  ClassDef(StKnockoutProtonMaker,1);

};

#endif
