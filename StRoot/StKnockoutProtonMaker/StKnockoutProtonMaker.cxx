#include <iostream>
#include <cstdio>
#include <vector>

#include "TObject.h"
#include "TFile.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"

#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuDst.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"
#include "StMuDSTMaker/COMMON/StMuTrack.h"
#include "StTriggerData.h"
#include "StTriggerIdCollection.h"
#include "StRunInfo.h"

#include "StRoot/StRefMultCorr/StRefMultCorr.h"
#include "StRoot/StRefMultCorr/CentralityMaker.h"

#include "StRoot/StKnockoutProtonMaker/StKnockoutProtonMaker.h"

ClassImp(StKnockoutProtonMaker);

//_________________________________________________________
StKnockoutProtonMaker::StKnockoutProtonMaker(char *name): StMaker(name){


}

//_________________________________________________________
StKnockoutProtonMaker::~StKnockoutProtonMaker(){

  

}

//__________________________________________________________
Int_t StKnockoutProtonMaker::Init(){

  cout <<"Starting Init()" <<endl;

  //Set the Output File Name
  TString fileName = fileNameBase;
  fileName.Append(mFileIndex);
  fileName.Append(".root");
  fileName.Prepend(mOutDir);

  cout <<"Working on output file: " <<fileName <<endl;

  //Create the outout Root File
  outFile = new TFile(fileName.Data(), "RECREATE");
  
  



  cout <<"Init() Successful" <<endl;

  return kStOK;
}

//___________________________________________________________
void StKnockoutProtonMaker::Clear(const char *c){

  
}

//___________________________________________________________
Int_t StKnockoutProtonMaker::Make(){

  //The MuDst
  StMuDstMaker *muDstMaker = NULL;
  muDstMaker = (StMuDstMaker *)GetMaker("MuDst");
  if (!muDstMaker){
    fputs("ERROR: StKnockoutProtonMaker::Init() - Can't get pointer to StMuDstMaker!", stderr);
    return kStFATAL;
  }

  StMuDst *mMuDst = NULL;
  mMuDst = muDstMaker->muDst();
  if (!mMuDst){
    fputs("ERROR: StKnockoutProtonMaker::Init() - Can't get pointer to StMuDst!", stderr);
    return kStFATAL;
  }
  
  //StRefMultCorr Bad Run Rejection
  StRefMultCorr *refMultCorr = CentralityMaker::instance()->getRefMultCorr();
  if (refMultCorr->isBadRun(mMuDst->event()->runNumber()))
    return kStOK;
  




  return kStOK;
}

//___________________________________________________________
Int_t StKnockoutProtonMaker::Finish(){

  return kStOK;
}


